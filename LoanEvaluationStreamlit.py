# -*- coding: utf-8 -*-

import streamlit as st
import numpy as np
import pandas as pd
import xgboost as xgb

model = xgb.XGBClassifier()
model.load_model("finalmodel.json")
    

st.header("LoanPro")
st.write("The best loan evaluation tool, maybe ever.")
st.write("Birthed by Belisprinsen and Ballonggreven.")

st.text("")

st.write("Please enter your info.")

Married = st.selectbox("Are you married?", ("-Choose-", "Yes", "No"))
Gender = st.selectbox("What is your gender?", ("-Choose-", "Male", "Female"))
Dependents = st.selectbox("Number of people under 18 in household.", ("-Choose-", "0", "1", "2", "3+"))
Education = st.selectbox("Level of education", ("-Choose-", "Graduate", "Not Graduate"))
Self_employed = st.selectbox("Are your self employed?", ("-Choose-", "Yes", "No"))
ApplicantIncome = st.number_input("Monthly income in dollarydoos.", step=1)
CoapplicantIncome = st.number_input("Monthly income of coapplicant in dollarydoos.", step=1)
LoanAmount = st.number_input("Desired loan amount.", step=1)
LoanAmount = LoanAmount/1000
LoanAmountTerm = st.selectbox("Loan duration in months.", ("-Choose-", 12, 36, 60, 84, 120, 180, 240, 300, 360, 480))
CreditHistory = st.selectbox("Does your credit history meet the loan requirement?", ("-Choose-", "Yes", "No"))
PropertyArea = st.selectbox("Where is your property located?", ("-Choose-", "Semiurban", "Urban", "Rural"))


data = [[ApplicantIncome, CoapplicantIncome, LoanAmount, LoanAmountTerm, 
         CreditHistory, Gender, Married, Dependents, 0, 0, 0, 0, Education, 
         Self_employed, PropertyArea, 0, 0, 0]]
columns =  ['ApplicantIncome', 'CoapplicantIncome', 'LoanAmount', 'Loan_Amount_Term', 
            'Credit_History', 'Gender', 'Married', 'Dependents', 'Dependents_0', 
            'Dependents_1', 'Dependents_2', 'Dependents_3+', 'Education', 
            'Self_Employed', 'PropertyArea', 'Property_Area_Rural', 
            'Property_Area_Semiurban', 'Property_Area_Urban']

df = pd.DataFrame(data=data, columns=columns)

# turning info into binary
df['Married'] = df['Married'].map({"Yes":1, "No":0})
df['Gender'] = df['Gender'].map({"Male":1, "Female":0})
df['Education'] = df['Education'].map({"Graduate":1, "Not Graduate":0})
df['Self_Employed'] = df['Self_Employed'].map({"Yes":1, "No":0})
df['Credit_History'] = df['Credit_History'].map({"Yes":1, "No":0})

# one hot encoding the 'dependents' column to match the training data
Dependents_0, Dependents_1, Dependents_2, Dependents_3 = 0, 0, 0, 0
if Dependents == "0": Dependents_0 = 1
if Dependents == "1": Dependents_1 = 1
if Dependents == "2": Dependents_2 = 1
if Dependents == "3+": Dependents_3 = 1    
df = df.drop("Dependents", axis=1)
df['Dependents_0'], df['Dependents_1'], df['Dependents_2'], df['Dependents_3+'] = Dependents_0, Dependents_1, Dependents_2, Dependents_3

# one hot encoding the 'Property_Area' column to match the training data
Property_Area_Rural, Property_Area_Semiurban, Property_Area_Urban = 0, 0, 0
if PropertyArea == "Rural": Property_Area_Rural = 1
if PropertyArea == "Semiurban": Property_Area_Semiurban = 1
if PropertyArea == "Urban": Property_Area_Urban = 1
df = df.drop("PropertyArea", axis=1)
df["Property_Area_Rural"], df["Property_Area_Semiurban"], df["Property_Area_Urban"] = Property_Area_Rural, Property_Area_Semiurban, Property_Area_Urban

# Squaring these columns, as the model is fitted on squared data
df['ApplicantIncome'] = np.sqrt(df['ApplicantIncome'])
df['CoapplicantIncome'] = np.sqrt(df['CoapplicantIncome'])

# alternatively, this preprocessing of data could be made easier with fit_transform()

if st.button("Send application"):
    pred = model.predict(df)
    if pred == 1:    
        st.write(f"Congratulations! I can give you a small loan of {LoanAmount*1000} USD for {LoanAmountTerm} months!")
    elif pred == 0:
        st.write(f"Oh no! I can NOT give you a small loan of {LoanAmount*1000} USD for {LoanAmountTerm} months")
