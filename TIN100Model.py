#!/usr/bin/env python
# coding: utf-8

# Imports

import pandas as pd
import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

from imblearn.over_sampling import SMOTE
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
import xgboost as xgb
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import make_pipeline

from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix


# import data
df = pd.read_csv("train.csv")


def VisualizeLoans(df):
    
    df.head()
    print(df.shape)
    df[['ApplicantIncome','CoapplicantIncome','LoanAmount']].describe()

    # Check distribution of male vs. female
    df.Gender.value_counts(dropna=False)
    
    # plotting binary colomns to order to compare counts
    fig, axs = plt.subplots(2, 2, figsize=(12, 10))
    sns.countplot(x="Loan_Status", data=df, ax=axs[0, 0], palette='hls')
    sns.countplot(x="Gender", data=df, ax=axs[1, 0], palette='mako')
    sns.countplot(x="Credit_History", data=df, ax=axs[0, 1], palette='magma')
    sns.countplot(x="Married", data=df, ax=axs[1, 1], palette='rocket_r')
    
    # Visualize numerical data, and loan_amount_term
    sns.set(style="darkgrid")
    fig, axs = plt.subplots(2, 2, figsize=(12, 10))
    
    sns.histplot(data=df, x='ApplicantIncome', kde=True, ax=axs[0, 0], color='green')
    sns.histplot(data=df, x='CoapplicantIncome', kde=True, ax=axs[0, 1], color='skyblue')
    sns.histplot(data=df, x='LoanAmount', kde=True, ax=axs[1, 0], color='orange')
    sns.histplot(data=df, x='Loan_Amount_Term', ax=axs[1, 1], color='red')




def PreprocessLoans(df):
    # drop ID column as it serves no purpose
    df = df.drop(["Loan_ID"], axis=1)
        
    # fill missing values with the most common value for categorical data
    # missing numerical data gets filled with the mean value of the column
    df['Gender'].fillna(df['Gender'].mode()[0],inplace=True)
    df['Married'].fillna(df['Married'].mode()[0],inplace=True)
    df['Dependents'].fillna(df['Dependents'].mode()[0],inplace=True)
    df['Self_Employed'].fillna(df['Self_Employed'].mode()[0],inplace=True)
    df['Credit_History'].fillna(df['Credit_History'].mode()[0],inplace=True)
    df['Loan_Amount_Term'].fillna(df['Loan_Amount_Term'].mode()[0],inplace=True)
    df['LoanAmount'].fillna(df['LoanAmount'].mean(),inplace=True)    

    # One-hot encoding
    # use dummies function to one-hot encode categorical data
    df = pd.get_dummies(df)
    df = df.drop(['Gender_Female', "Married_No", "Education_Not Graduate", "Self_Employed_No", "Loan_Status_N"], axis=1)
    new = {"Gender_Male": "Gender", "Married_Yes": "Married", "Education_Graduate": "Education", 
           "Self_Employed_Yes": "Self_Employed", "Loan_Status_Y": "Loan_Status"}
    df.rename(columns=new, inplace=True)

    # Removing outliers
    # cut 10% of data on each side of numerical data. This removes many outliers making the data easier to model.
    numerical = ['ApplicantIncome', 'CoapplicantIncome', 'LoanAmount']
    Q1 = df[numerical].quantile(0.10)
    Q3 = df[numerical].quantile(0.90)
    IQR = Q3 - Q1
    
    df[numerical] = df[~((df[numerical] > Q3+1.5*IQR) | (df[numerical] < Q1 - 1.5*IQR))][numerical]
    df = df.dropna().reset_index().drop('index', axis=1)

    # Take the root of these numerical columns make them easier to model.
    df['ApplicantIncome'] = np.sqrt(df['ApplicantIncome'])
    df['CoapplicantIncome'] = np.sqrt(df['CoapplicantIncome'])

    # visualize procesed data.
    #sns.set(style="darkgrid")
    #fig, axs = plt.subplots(2, 2, figsize=(12, 10))
    
    #sns.histplot(data=df, x='ApplicantIncome', kde=True, ax=axs[0, 0], color='green')
    #sns.histplot(data=df, x='CoapplicantIncome', kde=True, ax=axs[0, 1], color='skyblue')
    #sns.histplot(data=df, x='LoanAmount', kde=True, ax=axs[1, 0], color='orange')
    #sns.histplot(data=df, x='Loan_Amount_Term', ax=axs[1, 1], color='red')

    # split for more preprocessing
    X = df.drop(["Loan_Status"], axis=1)
    y = df["Loan_Status"]

    # upsampling the target variable
    X, y = SMOTE(sampling_strategy='minority').fit_resample(X, y)
    
    # Test-Train-Split
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size = 0.2, stratify=y, random_state=0)

    return df, X_train, X_val, y_train, y_val, X, y



def FeatureImportanceLoans(X_train, y_train, X):
    # creating classifier for feature selection
    forest = RandomForestClassifier(n_estimators=500,
                                    random_state=1,
                                    n_jobs=-1)
    forest.fit(X_train, y_train)
    
    # running the feature importance from the forest classifier
    importances = forest.feature_importances_
    feat_labels = X.columns
    indices = np.argsort(importances)[::-1]
    
    for f in range(X_train.shape[1]):
        print("%2d) %-*s %f" % (f + 1, 30, 
                                feat_labels[indices[f]], 
                                importances[indices[f]]))
    
    # Plot feature importances
    plt.figure(figsize = (15,8))
    plt.title('Feature Importance')
    plt.bar(range(X_train.shape[1]), 
            importances[indices],
            align='center')
    
    plt.xticks(range(X_train.shape[1]), 
               feat_labels[indices], rotation=90)
    plt.xlim([-1, X_train.shape[1]])
    plt.tight_layout()
    #plt.savefig('images/04_09.png', dpi=300)
    plt.show()

 
def SVC_loans(X_train, X_val, y_train, y_val):
    
    # Define ranges of parameter values:
    #param_range  = [0.01, 1.0, 10.0, 100.0, 1000.0, 10000.0]   # For regularization parameter C.
    #param_range2 = [0.001, 0.01, 0.1, 1.0, 10.0, 100.0]        # For scaling parameter gamma og rbf-kernel.
    
    param_range  = [1.0, 10.0]   # For regularization parameter C.
    param_range2 = [0.01, 0.1]        # For scaling parameter gamma og rbf-kernel.
    
    
    param_grid   = [{'svc__C': param_range, 'svc__kernel': ['linear']},
                    {'svc__C': param_range, 'svc__gamma': param_range2, 'svc__kernel': ['rbf']}]
    
    # Pipe for model
    pipe_svc = make_pipeline(StandardScaler(), SVC(random_state=1))
    
    # Grid search for best parameters
    SVCgs = GridSearchCV(estimator=pipe_svc, 
                      param_grid=param_grid, 
                      scoring='accuracy', 
                      cv=10,
                      n_jobs=-1)
    
    SVCgs = SVCgs.fit(X_train, y_train)
    print(SVCgs.best_score_)
    print(SVCgs.best_params_)

    # Using best parameters for predicting test set
    SVC_model = SVCgs.best_estimator_
    SVC_model.fit(X_train, y_train)
    SVC_pred = SVC_model.predict(X_val)
    SVCacc = accuracy_score(SVC_pred, y_val)

    print('____________Support Vector Classification____________')
    print(classification_report(SVC_pred, y_val))
    print(confusion_matrix(SVC_pred, y_val))
    print("SVC Accuracy: {:.2f}%".format(SVCacc*100))
    
    # make heat map of best parameters
    scores = SVCgs.cv_results_['mean_test_score'][len(param_range):].reshape(len(param_range),len(param_range2))
    plt.figure(figsize=(8, 6))
    plt.subplots_adjust(left=.2, right=0.95, bottom=0.15, top=0.95)
    plt.imshow(scores, interpolation='nearest', cmap=plt.cm.hot)
    plt.xlabel('gamma') # 1/(softness of decision boundary), i.e. small = soft
    plt.ylabel('C')     # 1/(size of support vector margin), i.e. large = tight margin
    plt.colorbar()
    plt.xticks(np.arange(len(param_range2)), param_range2)
    plt.yticks(np.arange(len(param_range)), param_range)
    plt.title('Grid Search Accuracy Score')
    plt.show()
    
    return SVCacc


def RandomForest_loans(X_train, X_val, y_train, y_val):
    
    # random forest. check best value for max_leaf_nodes.
    scoreListRF = []
    for i in range(2, 25):
        RFclassifier = RandomForestClassifier(n_estimators = 1000, random_state = 1, max_leaf_nodes=i)
        RFclassifier.fit(X_train, y_train)
        scoreListRF.append(RFclassifier.score(X_val, y_val))
    
    plt.figure(figsize=(8, 6))
    plt.plot(range(2,25), scoreListRF)
    plt.xticks(np.arange(2,25,1))
    plt.xlabel("RF Value")
    plt.ylabel("Score")
    plt.show()
    RFpred = max(scoreListRF)
    print("Random Forest Accuracy:  {:.2f}%".format(RFpred*100))

    # make model with best parameter. Randon forest generally does not overfit, so large value is better.
    RFmodel = RandomForestClassifier(n_estimators = 1000, random_state = 1, max_leaf_nodes=1000)
    RFmodel.fit(X_train, y_train)
    RF_pred = RFmodel.predict(X_val)
    RFacc = accuracy_score(RF_pred, y_val)
        
    print('____________________Random Forest____________________')
    print(classification_report(RF_pred, y_val))
    print(confusion_matrix(RF_pred, y_val))
    print("Random Forest Accuracy:  {:.2f}%".format(RFacc*100))

    return RFacc

def LogisticRegression_loans(X_train, X_val, y_train, y_val):

    # grid search over parameters
    param_range = [0.1, 0.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20,]
    param_range2 = ['l1', 'l2', 'elasticnet', 'None']
    
    param = [{'penalty': param_range2, 'C': param_range}]
    
    LRclassifier = LogisticRegression(random_state=1)
    
    LRgs = GridSearchCV(LRclassifier,
                 param_grid=param, 
                 cv=5,
                 refit=True)
    
    LRgs.fit(X_train, y_train)
    
    # use best estimator from grid search to predict.
    LR_model = LRgs.best_estimator_
    LR_pred= LR_model.predict(X_val)
    
    print(LRgs.best_params_)
    
    print('_________________Logistic Regression_________________')
    print(classification_report(LR_pred, y_val))
    print(confusion_matrix(LR_pred, y_val))
    LRacc = accuracy_score(LR_pred, y_val)
    print('LR accuracy: {:.2f}%'.format(LRacc*100))

    return LRacc

def XGBoost_loans(X_train, X_val, y_train, y_val):
    
    # make grid for gridsearch
    param_grid = {
            'min_child_weight': [1],
            'gamma': [0.5, 1, 1.5, 2],
            'subsample': [1],
            'colsample_bytree': [0.5],
            'max_depth': [5, 10, 15],
            'learning_rate': [0.001, 0.01, 0.1]
            }
    
    
    # grid search over possible parameters. Narrow in grid after search for optimal paramters.
    estimator = xgb.XGBClassifier(n_estimators=3000, 
                                  n_jobs=-1, 
                                  #eval_metric='logloss',
                                  #early_stopping_rounds=50,
                                  #objective= 'binary:logistic',
                                  max_depth=3000)
    
    eval_set = [(X_train, y_train), (X_val, y_val)]
    
    XGBgs = GridSearchCV(estimator=estimator,
                         param_grid=param_grid,
                         cv=3,
                         scoring="accuracy")
    
    XGBgs.fit(X_train, y_train, eval_set=eval_set, verbose=0)
    
    
    print(XGBgs.best_params_)

    
    # use the best estimator for final prediction
    XGB_model = XGBgs.best_estimator_
    
    XGB_model.fit(X_train, y_train, eval_set=eval_set, verbose=0)
    
    XGB_pred = XGB_model.predict(X_val)
    
    
    results = XGB_model.evals_result()
    
    # plot learning curves
    plt.plot(results['validation_0']['logloss'], label='train')
    plt.plot(results['validation_1']['logloss'], label='test')
    # show the legend
    plt.legend()
    # show the plot
    plt.ylabel("Classification Error")
    plt.xlabel("Number of boosting rounds")
    plt.title("XGBoost Classification Error")
    plt.show()
    
    print('_______________________XGBoost_______________________')
    print(classification_report(y_val, XGB_pred))
    print(confusion_matrix(y_val, XGB_pred))
    
    XGBacc = accuracy_score(XGB_pred, y_val)
    print('XBG accuracy: {:.2f}%'.format(XGBacc*100))
    
    return XGBacc
    
def XGB_finalModel_loans(X_train, y_train):
    estimator = xgb.XGBClassifier(n_estimators=3000,
                                  n_jobs=-1, 
                                  #eval_metric='logloss',
                                  #early_stopping_rounds=50,
                                  #objective= 'binary:logistic',
                                  max_depth=3000,
                                  min_child_weight=1,
                                  gamma=1,
                                  subsample=1,
                                  colsample_bytree=0.5,
                                  learning_rate=0.01)
    estimator.fit(X_train, y_train)
    
    return estimator


def ModelComparison(LRacc, XGBacc, SVCacc, RFacc):

    # df for comparing model performance
    compare = pd.DataFrame({'Model': ['Logistic Regression', 
                                      'XGBoost', 
                                      'SVC', 
                                      'Random Forest'], 
                            'Accuracy': [LRacc*100, 
                                         XGBacc*100, 
                                         SVCacc*100, 
                                         RFacc*100]})
    print(compare.sort_values(by='Accuracy', ascending=False))


def runAllModels(df):
    
    df, X_train, X_val, y_train, y_val, X, y = PreprocessLoans(df)
    
    FeatureImportanceLoans(X_train, y_train, X)
                           
    SVCacc = SVC_loans(X_train, X_val, y_train, y_val)
    RFacc = RandomForest_loans(X_train, X_val, y_train, y_val)
    LRacc = LogisticRegression_loans(X_train, X_val, y_train, y_val)
    XGBacc = XGBoost_loans(X_train, X_val, y_train, y_val)
    ModelComparison(LRacc, XGBacc, SVCacc, RFacc)

#runAllModels(df)

def save_model(df):
    df, X_train, X_val, y_train, y_val, X, y = PreprocessLoans(df)
    model = XGB_finalModel_loans(X_train, y_train)
    model.save_model("finalmodel.json")

save_model(df)


def preprocess_streamlit(df):
    # One-hot encoding
    # use dummies function to one-hot encode categorical data
    df = pd.get_dummies(df)
    df = df.drop(['Gender_Female', "Married_No", "Education_Not Graduate", "Self_Employed_No", "Loan_Status_N"], axis=1)
    new = {"Gender_Male": "Gender", "Married_Yes": "Married", "Education_Graduate": "Education", 
           "Self_Employed_Yes": "Self_Employed", "Loan_Status_Y": "Loan_Status"}
    df.rename(columns=new, inplace=True)
    
    df['ApplicantIncome'] = np.sqrt(df['ApplicantIncome'])
    df['CoapplicantIncome'] = np.sqrt(df['CoapplicantIncome'])
    return df

def model_predict(model, X):
    pred = model.predict(X)
    return pred
    
