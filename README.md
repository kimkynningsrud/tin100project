# TIN100Project


## ABSTRACT
This report presents a machine learning project that aims to improve the loan application evaluation process for lenders. The goal is to create a model that can predict the likelihood of loan repayment, enabling loan providers to make more informed lending decisions and in turn make the process more efficient and accurate. This will ultimately increase access to credit for individuals and small businesses and create value for loan providers by reducing the risk of default loans. 

The project involves collecting and preparing a dataset of past loan information, including the borrower's personal and financial information, combined with the desired loan amount and term length. This is a binary classification problem with both numerical and categorical data in tabular format. The project team is made up of two students from the Norwegian University of Life Sciences, both studying a master's degree in Data Science with specialization in Business Analytics. The team found XGBoost to be the best model for the current problem, with an accuracy of around 90% on the test set. The team has developed a Streamlit prototype integrating the best performing model, allowing for evaluation of user input data. 

The report concludes that the best alternative is dependent on the risk-taking willingness of the lender; alternative 0 is the overall safest option, but it will not improve on costs or efficiency. Semi-automation will likely fit best to an established lender, while full automation is the riskiest option but is likely give the best results in terms of cost and efficiency, provided sufficient management. 
